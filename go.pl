#!/usr/bin/perl
use DBI;
use Encode;
use strict;
use warnings;

open(OUT, ">out.stl");

for (my $cnt=0;$cnt<80;$cnt++) {
    print OUT chr(0);
}

my $cnt = 1;

my @dots = ();

my %xs = ();
my %ys = ();

open(FILE, "<in.txt");

while(<FILE>) {
    m/([-+]?[0-9]*\.?[0-9]+)\,([-+]?[0-9]*\.?[0-9]+)\,([-+]?[0-9]*\.?[0-9]+)/;
    print "cnt=$cnt\tx=$1\ty=$2\tz=$3\n";

    my $dots2 = {id => $cnt, x => $1, y => $2, z => $3};
    push @dots, $dots2;

    push (@{$xs{$1}}, $1) unless exists $xs{$1};
    push (@{$ys{$2}}, $2) unless exists $ys{$2};
    $cnt++;
}

close FILE;

my $xs1 = keys(%xs);
my $ys1 = keys(%ys);

print "xs1=".$xs1."\n";
print "ys1=".$ys1."\n";


print OUT pack('L', (($xs1-1)*($ys1-1)*2));

my $cnt2 = 0;

for(my $yi=0;$yi<($ys1-1);$yi++) {
        for(my $xi=0;$xi<($xs1-1);$xi++) {
                print "######### $xi\t$yi\t$cnt2\t$xs1\t$ys1\n";
                $cnt2++;
                my $i1 = ($xi+($yi*$xs1));
                my $i2 = (($xi+1)+($yi*$xs1));
                my $i3 = ($xi+(($yi+1)*$xs1));
                my $i4 = (($xi+1)+(($yi+1)*$xs1));
                print "id=".$dots[$i1]->{id}."\ti1=$i1\tx=".$dots[$i1]->{x}."\ty=".$dots[$i1]->{y}."\tz=".$dots[$i1]->{z}."\n";
                print "id=".$dots[$i2]->{id}."\ti2=$i2\tx=".$dots[$i2]->{x}."\ty=".$dots[$i2]->{y}."\tz=".$dots[$i2]->{z}."\n";
                print "id=".$dots[$i3]->{id}."\ti3=$i3\tx=".$dots[$i3]->{x}."\ty=".$dots[$i3]->{y}."\tz=".$dots[$i3]->{z}."\n";
                print "id=".$dots[$i4]->{id}."\ti4=$i4\tx=".$dots[$i4]->{x}."\ty=".$dots[$i4]->{y}."\tz=".$dots[$i4]->{z}."\n";

                #normal vector
                print OUT pack('f', 0.0);
                print OUT pack('f', 0.0);
                print OUT pack('f', 0.0);
                #vertex1
                print OUT pack('f', $dots[$i1]->{x});
                print OUT pack('f', $dots[$i1]->{y});
                print OUT pack('f', $dots[$i1]->{z});
                #vertex2
                print OUT pack('f', $dots[$i2]->{x});
                print OUT pack('f', $dots[$i2]->{y});
                print OUT pack('f', $dots[$i2]->{z});
                #vertex3
                print OUT pack('f', $dots[$i3]->{x});
                print OUT pack('f', $dots[$i3]->{y});
                print OUT pack('f', $dots[$i3]->{z});
                #attribute count
                print OUT pack('n', 0x1);

                #normal vector
                print OUT pack('f', 0.0);
                print OUT pack('f', 0.0);
                print OUT pack('f', 0.0);
                #vertex1
                print OUT pack('f', $dots[$i2]->{x});
                print OUT pack('f', $dots[$i2]->{y});
                print OUT pack('f', $dots[$i2]->{z});
                #vertex2
                print OUT pack('f', $dots[$i4]->{x});
                print OUT pack('f', $dots[$i4]->{y});
                print OUT pack('f', $dots[$i4]->{z});
                #vertex3
                print OUT pack('f', $dots[$i3]->{x});
                print OUT pack('f', $dots[$i3]->{y});
                print OUT pack('f', $dots[$i3]->{z});
                #attribute count
                print OUT pack('n', 0x1);

        }
        print "-------------\n";
}

print "TRIANGLES: ".(($xs1-1)*($ys1-1)*2)."\n";

close OUT;

exit();
